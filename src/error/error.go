package main

import (
	"fmt"
	"math"
)

// As with fmt.Stringer, the fmt package looks for the error interface when printing values.
// A nil error denotes success; a non-nil error denotes failure.

type ErrNegativeSqrt float64

func (e ErrNegativeSqrt) Error() string {
	return fmt.Sprintf("cannot Sqrt negative number: %v", float64(e))
}

func Sqrt(x float64) (float64, error) {
	z := float64(2.)
	s := float64(0)
	if x > 0 {
		for {
			z = z - (z*z - x)/(2*z)
			if math.Abs(s-z) < 1e-15 {
				break
			}
			s = z
		}
		return s, nil
	} else {
		return 0, ErrNegativeSqrt(x)
	}
}

func main() {
	fmt.Println(Sqrt(2))
	fmt.Println(Sqrt(-2))
}
