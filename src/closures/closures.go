package main

import "fmt"

func fibonnaci() func() int {
  fibSequence := []int{0,1,1}
  count := 0
  return func() int {
    count++
    if len(fibSequence) <= count {
      newNum := fibSequence[count-1] + fibSequence[count-2]
      fibSequence = append(fibSequence, newNum)
    }
    return fibSequence[count]
  }
}

func main() {
  f := fibonnaci()
  t := fibonnaci()
  for i := 0; i < 10; i++ {
		fmt.Println(f())
	}
  for i := 0; i < 10; i++ {
		fmt.Println(t())
	}
}
