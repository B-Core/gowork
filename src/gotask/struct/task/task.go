package task

import(
  "time"
  "fmt"
  "crypto/rand"
)

/**
* generate a random pseudo uuid
* @return
* uuid {string}
*/

func pseudo_uuid() (uuid string) {

    b := make([]byte, 16)
    _, err := rand.Read(b)
    if err != nil {
        fmt.Println("Error: ", err)
        return
    }

    uuid = fmt.Sprintf("%X-%X-%X-%X-%X", b[0:4], b[4:6], b[6:8], b[8:10], b[10:])

    return
}

type Task struct {
  Uuid string
  Title string
  Description string
  Tags []string
  Priority int
  Pending bool
  Finished bool
  CreatedAt time.Time
  Duration time.Duration
}

func Create(title string, description string, tags []string, priority int) *Task {
  createDate := time.Now()
  duration := time.Since(time.Now())

  task := Task{
    pseudo_uuid(),
    title,
    description,
    tags,
    priority,
    true,
    false,
    createDate,
    duration,
  }

  return &task
}

func(task *Task) Finish() {
  if task.Finished == false {
    task.Pending = false
    task.Finished = true
    task.CalculateDuration()
  } else {
    fmt.Printf("task %s already finished\n", task.Title)
  }
}

func(task *Task) CalculateDuration() {
  task.Duration = time.Since(task.CreatedAt)
}
