package rooter

import(
  "strconv"
  "strings"
  "github.com/fatih/color"
  "gotask/commands"
)

func Root(path string, args []string) {
  commands.Clear()

  lenArgs := len(args)

  if commands.ArrayTextContains(args, "--finished") || commands.ArrayTextContains(args, "--f") {
    commands.SetListPending(true)
    args = args[:lenArgs-1]
  } else {
    commands.SetListPending(false)
  }

  switch path {
    case "list" :
      commands.List(args)
      break
    case "add" :
      switch len(args) {
        case 0 :
          color.Red("Missing title, description, tags and priority(last is optionnal)")
          break
        case 1 :
          color.Red("Missing description, tags and priority(last is optionnal)")
          break
        case 2 :
          color.Red("Missing tags and priority(last is optionnal)")
          break
        default :
          if len(args) == 3 {
            color.Red("Missing priority default is 1")
          }
          priority := 1
          if lenArgs == 4 {
            priority, _ = strconv.Atoi(args[3])
          }
          commands.Create(args[0], args[1], strings.Split(args[2],","),priority)
      }
      break
    case "delete" :
      switch len(args) {
        case 0 :
          color.Red("Missing arguments")
          break
        case 1 :
          tasks := commands.Search([]string{"title",}, args[0:])
          commands.Delete(tasks)
        default :
          flags, flagArgs := commands.ExtractFlags(args)
          tasks := commands.Search(flags, flagArgs)
          commands.Delete(tasks)
      }
      break
    case "finish" :
      tasks := commands.Search([]string{"title",}, args[0:])
      commands.Finish(tasks)
      break
    case "help" :
      if(lenArgs > 0) {
        commands.PrintHelp(args[0])
      } else {
        commands.PrintHelp("all")
      }
    default :
      commands.List(args)
  }
}
