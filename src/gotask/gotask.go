package main

import(
  "os"
  "gotask/rooter"
)

func main() {
  lenArgs := len(os.Args)

  switch lenArgs {
  case 1 :
    rooter.Root("help", make([]string,0))
    break
  case 2 :
    rooter.Root(os.Args[1], make([]string,0))
    break
  default :
    args := os.Args[1:]
    rooter.Root(args[0], args[1:])
  }
}
