package commands

import(
  "io/ioutil"
  "os"
  "fmt"
  "encoding/json"
  "strings"
  "strconv"
  "log"
  "sort"
  "time"
  "path/filepath"
  "github.com/fatih/color"
  "github.com/kardianos/osext"
  tm "github.com/buger/goterm"
  "github.com/fatih/structs"
  "github.com/segmentio/go-prompt"
  "gotask/struct/task"
)

var folderPath, err = osext.ExecutableFolder()

var taskFolder = filepath.Join(folderPath, "../src/gotask/storage/") + "/"

var listFinished bool = false

/**
* Sort function interface
* Defining a type of sorting
* and defining sorting functions needed in the algorythm
*/

type ByPriority []*task.Task

func (a ByPriority) Len() int           { return len(a) }
func (a ByPriority) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByPriority) Less(i, j int) bool { return a[i].Priority < a[j].Priority }

/**
* Sets the listFinished var to true or false
* to determine which task to list
*/

func SetListPending(val bool) {
  listFinished = val
}

/**
* Create lines on all consoleWidth, but not quite working
*/

func CreateLines() {
  c := 0
  w := tm.Width()

  for c < w {
    fmt.Print("-")
    if(c == w-1) {
      fmt.Printf("-\n")
    }
    c++
  }
}

/**
* Clear the console with goterm
*/

func Clear() {
  tm.Clear()
  tm.Flush()
}

/**
* check if an array contains a string
* @params
* array {[]string} - the array of string
* s {string} - the string to search
* @return
* {bool}
*/

func ArrayTextContains(array []string, s string) bool {
    for _, a := range array {
        if a == s {
            return true
        }
    }
    return false
}

/**
* check if an array contains a task
* @params
* array {[]*Task} - the array of *Task
* task
* @return
* {bool}
*/

func arrayContains(array []*task.Task, task *task.Task) bool {
    for _, a := range array {
        if a.Title == task.Title {
            return true
        }
    }
    return false
}

/**
* Take an array of flags and returns the task keys equivalent
* @params
* flags {[]string} - the array of flags
* @return
* flags {[]string} - the transformed flags
*/

func transformFlags(flags []string) []string {
  transFlags := make([]string, 0)

  for _, flag := range flags {
    if flag == "prio" || flag == "p" || flag == "priority" {
      transFlags = append(transFlags, "priority")
    } else if flag == "d" || flag == "da" || flag == "date" {
      transFlags = append(transFlags, "createdAt")
    } else if flag == "ti" || flag == "t" || flag == "title" {
      transFlags = append(transFlags, "title")
    } else if flag == "ta" || flag == "tag" || flag == "tags" {
      transFlags = append(transFlags, "tags")
    } else if flag == "desc" || flag == "dc" || flag == "description" {
      transFlags = append(transFlags, "description")
    } else if flag == "dur" || flag == "duration" {
      transFlags = append(transFlags, "duration")
    }
  }

  return transFlags
}

/**
* Extract strings containing -- (aka flags) from string array
* @params
* array {[]string} - the array of string
* @return
* flags, flagsArgs {[]string} - respectively the flags and the arguments behind
*/

func ExtractFlags(array []string) ([]string, []string) {
  flags := make([]string, 0)
  flagArgs := make([]string, 0)

  for _, el := range array {
    if strings.Contains(el, "--") {
      flags = append(flags, strings.Replace(el, "--", "", 1))
    } else {
      flagArgs = append(flagArgs, el)
    }
  }

  return flags, flagArgs
}

func PrintHelp(command string) {
  cyanColor := color.New(color.FgCyan)
  greenColor := color.New(color.FgGreen)

  switch command {
  case "list" :
    cyanColor.Printf("\nlist\n\n")
    fmt.Println("lists all the task by priority order (less to most)")
    fmt.Print("Syntax : ")
    greenColor.Print("list [searchs]")
    fmt.Printf("\nexemple : list --title test --date '2016-08' = lists all tasks containing test in their title and having a createdAt date containing 2016-08\n\n")
    cyanColor.Println("flags : ")
    fmt.Println("   --title/--ti/--t : by title name (contain search not exact)")
    fmt.Println("   --date/--da/--d : by date (contain search not exact)")
    fmt.Println("   --tags/--tag/--ta : by tags (contain search not exact)")
    fmt.Println("   --description/--desc/--dc : by description (contain search not exact)")
    fmt.Println("   --duration/--dur : by duration (contain search not exact)")
    break
  case "delete" :
    cyanColor.Printf("\ndelete\n\n")
    fmt.Println("delete all tasks matching parameters")
    fmt.Print("Syntax : ")
    greenColor.Print("delete [title] | delete [searchs]")
    fmt.Printf("\nexemple : delete test | delete --title test --date '2016-08'\n\n")
    cyanColor.Println("flags : ")
    fmt.Println("   --title/--ti/--t : by title name (contain search not exact)")
    fmt.Println("   --date/--da/--d : by date (contain search not exact)")
    fmt.Println("   --tags/--tag/--ta : by tags (contain search not exact)")
    fmt.Println("   --description/--desc/--dc : by description (contain search not exact)")
    fmt.Println("   --duration/--dur : by duration (contain search not exact)")
    break
  case "add" :
    cyanColor.Printf("\nadd\n\n")
    fmt.Println("add a task")
    fmt.Print("Syntax : ")
    greenColor.Print("add [title] [description] [tags] [priority]")
    fmt.Printf("\nexemple : add test 'description' dev,test 2\n\n")
    break
  case "finish" :
    cyanColor.Printf("\nfinish\n\n")
    fmt.Println("finish all tasks matching parameters")
    fmt.Print("Syntax : ")
    greenColor.Print("finish [title] | finish [searchs]")
    fmt.Printf("\nexemple : finish test | finish --title test --date '2016-08'\n\n")
    cyanColor.Println("flags : ")
    fmt.Println("   --title/--ti/--t : by title name (contain search not exact)")
    fmt.Println("   --date/--da/--d : by date (contain search not exact)")
    fmt.Println("   --tags/--tag/--ta : by tags (contain search not exact)")
    fmt.Println("   --description/--desc/--dc : by description (contain search not exact)")
    fmt.Println("   --duration/--dur : by duration (contain search not exact)")
    break
  case "all" :
    fmt.Println("Gotask : console task manager")
    PrintHelp("list")
    PrintHelp("add")
    PrintHelp("delete")
    PrintHelp("finish")
  }
}

/**
* Pretty print a task
* @params
* task {*Task} - the task to print
*/

func Print(task *task.Task) {
  cyanColor := color.New(color.FgCyan)
  priorityColor := color.New(color.FgWhite)
  blueColor := color.New(color.FgBlue)
  magentaColor := color.New(color.FgMagenta)

  fmt.Printf("%s\n", task.Uuid)
  cyanColor.Printf("%s\n", task.Title)
  fmt.Printf("%s\n\n", task.Description)

  switch task.Priority {
    case 0 :
      priorityColor = priorityColor.Add(color.BgBlue)
      break
    case 1 :
      priorityColor = priorityColor.Add(color.BgCyan)
      break
    case 2 :
      priorityColor = priorityColor.Add(color.BgYellow)
      break
    case 3 :
      priorityColor = priorityColor.Add(color.BgRed)
      break
  }

  fmt.Print("PRIORITY : ")
  priorityColor.Printf("  %v  \n", task.Priority)

  fmt.Print("TAGS : ")
  blueColor.Printf("%s\n", task.Tags)
  fmt.Print("CreatedAt : ")
  blueColor.Printf("%s\n", task.CreatedAt)

  statusColor := color.New(color.FgRed)

  if task.Pending {
    statusColor = statusColor.Add(color.BgYellow)
    fmt.Print("STATUS : ")
    statusColor.Println("  Pending  ")
  } else {
    statusColor = statusColor.Add(color.BgGreen)
    fmt.Print("STATUS : ")
    statusColor.Println("  finished  ")
  }

  if(task.Pending) {
    task.CalculateDuration()
  }

  fmt.Print("Duration : ")
  magentaColor.Printf("%s\n\n", task.Duration)
}

/**
* Pretty print an array of task
* @params
* tasks {[]*Task} - the tasks to print
*/

func PrintTasks(tasks []*task.Task) {
  CreateLines()
  for index, task := range tasks {
    fmt.Print(strconv.Itoa(index) + ")")
    Print(task)
    CreateLines()
  }
}

/**
* Create a new Task and pass it to Save
* @params
* title {string} - the title of the task which will serve as a filename
* description {string} - the description
* tags {[]string} - the tags associated with ths task
* priority {int} - priority level
* @return *Task {Task}
*/

func Create(title string, description string, tags []string, priority int) *task.Task {
  tasks := Search([]string{"all",}, make([]string, 0))
  task := task.Create(title, description, tags, priority)

  if arrayContains(tasks, task) {
    color.Red("task %s already exists, change your title\n\n", task.Title)
    return task
  }

  color.Green("you just created %s\n\n", task.Title)
  fmt.Printf("Title : %s\nDescription : %s\npriority : %v\ntags : %s\ncreatedAt : %v\n\n", task.Title, task.Description, task.Priority,task.Tags, task.CreatedAt)

  return Save(task)
}

/**
* Save the Task in a json File
* @params
* filename {string} - the path to the file and its extension
* @return *Task {Task}
*/

func Save(task *task.Task) *task.Task {
  jData , err := json.Marshal(task)

  if err != nil {
    color.Red("ERROR with JSON conversion : ", err)
    return nil
  }

  filename := taskFolder + strings.Replace(task.Title, " ", "", -1) + ".json"

  ioutil.WriteFile(filename, jData, 0600)

  return task
}

/**
* Loads a Task from a json file
* @params
* title {string} - the title of the task which will serve as a filename
* @return *Task {Task}
*/

func Load(title string) (*task.Task, error) {
  filename := taskFolder + strings.Replace(title, " ", "", -1) + ".json"

  var task task.Task

  taskData, err := ioutil.ReadFile(filename)

  if err != nil {
    color.Red("ERROR : Could not read " + filename)
    return nil, err
  }

  err = json.Unmarshal(taskData, &task)

  if err != nil {
    color.Red("ERROR : Could not decode JSON data from " + filename)
    return nil, err
  }

  return &task, nil
}

/**
* delete a Task from system
* @params
* tasknames []string - the tasks to delete
*/

func Delete(tasks []*task.Task) {

  for _, task := range tasks {
    filename := taskFolder + strings.Replace(task.Title, " ", "", -1) + ".json"

    if ok := prompt.Confirm("are you sure you want to delete %s? (y/n)", task.Title); ok {
      err := os.Remove(filename)

      if err != nil {
        color.Red("ERROR in deleting file "+ filename)
      }

      color.Green("you just deleted %s\n\n", task.Title)
  	}
  }

  List(make([]string, 0))
}

/**
* finish a list of Tasks
* @params
* tasks []*Task
*/

func Finish(tasks []*task.Task) {
  for _, task := range tasks {
    if ok := prompt.Confirm("are you sure you want to finish %s? (y/n)", task.Title); ok {
      task.Finish()
      color.Green("finished task " + task.Title)
      Save(task)
    }
  }
}

/**
* list all a Tasks
*/

func List(args []string) {
  // fmt.Println(taskFolder)
  if len(args) == 0 {
    tasks := Search([]string{"all",}, make([]string, 0))
    sort.Sort(ByPriority(tasks))
    PrintTasks(tasks)
  } else {
    flags, flagArgs := ExtractFlags(args)

    response := Search(flags, flagArgs)
    sort.Sort(ByPriority(response))
    PrintTasks(response)
  }

}

/**
* treat flags and args and launch a search with searchTask
* @params
* flags {[]string} - the flags determining search keys
* search {[]string} - the flags arguments
* @return
* tasks {*[]Task}
*/

func Search(flags []string, search []string) []*task.Task {
  files, err := ioutil.ReadDir(taskFolder)

  if err != nil {
    log.Fatal(err)
  }

  tasks := make([]*task.Task, 0)

  for _, file := range files {
    task, _ := Load(strings.Replace(file.Name(), ".json", "", 1))
    if listFinished {
      tasks = append(tasks, task)
    } else {
      if task.Pending {
        tasks = append(tasks, task)
      }
    }
  }

  tasks = searchTask(tasks, flags, search)

  return tasks
}

/**
* search a specific task in an array of tasks
* @params
* tasks {[]*Task} - the array of tasks to search in
* flags {[]string} - the flags determining search keys
* search {[]string} - the flags arguments
* @return
* tasks {[]*Task}
*/

func searchTask(tasks[]*task.Task, flags []string, search []string) []*task.Task {
  retTasks := make([]*task.Task, 0)

  flags = transformFlags(flags)

  // fmt.Println(flags, search)

  for _, task := range tasks {
    mapTask := structs.Map(task)
    taskMatch := true
    tagFound := false

    for flagIndex, flag := range flags {
      if len(search) > 0 {
        args := strings.Split(search[flagIndex],",")
        key := strings.Title(flag)

        switch mapTask[key].(type) {
        case []string :
          for _, arg := range args {
            for _, tag := range task.Tags {
              if tag == arg {
                tagFound = true
                break
              }
            }
          }
          if !tagFound {
            taskMatch = false
          }
          // fmt.Println("task : " + task.Title, "args :" + arg, taskMatch, tagFound)
        case string :
          for _, arg := range args {
            if !strings.Contains(mapTask[key].(string), arg) {
              taskMatch = false
            }
            // fmt.Println("task : " + mapTask[key].(string), "args :" + arg, taskMatch)
          }
        case int :
          num := strconv.Itoa(mapTask[key].(int))
          for _, arg := range args {
            if num != arg {
              taskMatch = false
            }
          }
          // fmt.Println("task : " + num, "args :" + arg)
        case time.Time :
          t := task.CreatedAt.String()
          for _, arg := range args {
            if !strings.Contains(t, arg) {
              taskMatch = false
            }
            // fmt.Println("task.CreatedAt : " + t, "args :" + arg, taskMatch)
          }
        }
      }
    }
    if taskMatch && !arrayContains(retTasks, task) {
      retTasks = append(retTasks, task)
    }
  }

  return retTasks
}
