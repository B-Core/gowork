package main

import (
  "fmt"
  "math"
)

func powMax(x, n, max float64) float64 {
  // if statement can have an init statement
  // the variables declared in this statement is only disponible
  // till the end of the if loop
  if v := math.Pow(x, n); v < max {
    return v
  } else {
    fmt.Printf("%v >= %v\n", v, max)
  }
  return max
}

func main() {
  fmt.Println(
    powMax(3,1, 10),
    powMax(3,3, 10),
    powMax(3,3, 30),
  )
}
