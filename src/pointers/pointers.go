package main

import "fmt"

func main() {
  i, j := 42, 2701

  // p points to i
  p := &i
  fmt.Println(*p)

  // so to resume, & is to read through a variable
  // * is to set through variable or to read via print statement
  p = &j
  fmt.Println(*p)
  *p = 21
  fmt.Println(
    *p,
    j,
  )
}
