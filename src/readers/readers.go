// The io package specifies the io.Reader interface, which represents the read end of a stream of data.
package main

import (
  "fmt"
  "io"
  "strings"
)

// The io.Reader interface has a Read method:
// func (T) Read(b []byte) (n int, err error)
// Read populates the given byte slice with data and returns the number of bytes populated and an error value.
// It returns an io.EOF error when the stream ends.

func main() {
  reader := strings.NewReader("\n")
  numberOfByte := 16
  // create an array to consume the readers data numberOfByte byte at a time
  b := make([]byte, numberOfByte)

  for {
    // read the bytes
    n, err := reader.Read(b)
    fmt.Printf("n = %v err = %v b = %v\n", n, err, b)
    fmt.Printf("b[:%v] = %q\n", numberOfByte, b[:n])
    if err == io.EOF {
			break
		}
  }
}
