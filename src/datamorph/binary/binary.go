package binary

import(
  bytes "datamorph/bytestruct"
)

func Convert(conversionType string, value string, base int, byteLength int) (bytes.ByteNumber, string) {
  err := value + " is superior to 9223372036854775807 : cannot convert to byte\n"

  ret := bytes.ByteNumber{value,base,byteLength,0}
  d := 0

  if bytes.IsNumber(value) {
    d = bytes.ConvertStringToNumber(value)
    if d <= 9223372036854775807 {
      err = "nil"
    }
  } else {
    err = "nil"
  }

  if (bytes.IsNumber(value) && conversionType == "decimal/binary") {
    ret.FormatTo(conversionType, d);
  } else {
    ret.FormatTo(conversionType, value);
  }

  return ret, err
}
