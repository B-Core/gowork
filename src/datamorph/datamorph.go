package main

import(
  "github.com/mkideal/cli"
  "datamorph/binary"
  // "fmt"
)

type argT struct {
  cli.Helper
  Type string `cli:"*t,type" usage:"the type of conversion you want to perform [decimal/binary, binary/decimal, text/binary, binary/text]"`
  Value string `cli:"*v,value" usage:"the value to convert, can be int, float or string"`
  Base int `cli:"base,b" usage:"the base on which to convert the bytes" dft:"2"`
  BytesLength int `cli:"byteslength,bl" usage:"the length of each byte : 4,8" dft:"8"`
  PrintBoundaries int `cli:"p,printbounds" usage:"the boundaries to separate the byte on print" dft:"8"`
}

func main() {
  cli.Run(new(argT), func(ctx *cli.Context) error {
    argv := ctx.Argv().(*argT)
    // ctx.String("%s!\n", argv)
    res, err := binary.Convert(argv.Type, argv.Value, argv.Base, argv.BytesLength)
      if err != "nil" {
        ctx.String("%s", err)
      } else if argv.Type == "decimal/binary" || argv.Type == "text/binary" {
        ctx.String("%s\nBytes : %s  Base : %v  BytesLength : %v  NumberOfBytes : %v\n", res.ToString(argv.PrintBoundaries), res.Value, res.Base, res.ByteLength, res.NumberOfBytes)
      } else {
        ctx.String("%s", res.Value)
      }
    return nil
  })
}
