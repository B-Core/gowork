package bytestruct

import(
  "strconv"
  "math"
  byteLib "bytes"
  "encoding/binary"
  "fmt"
)

type ByteNumber struct {
  Value string
  Base int
  ByteLength int
  NumberOfBytes int
}

func convertIntToByte(value, base, byteLength int) string {
  b := value/base
  ret := strconv.Itoa(value%base)

  for {
    if (b / base) > 0 {
      ret += strconv.Itoa(b%base)
      b /= base
    } else {
      ret += strconv.Itoa(b%base)
      break
    }
  }

  ret = balanceByte(ret, byteLength)
  return ret
}

func convertStringToByte(value string, base, byteLength int) string {
  ret := ""

  for _, r := range value {
    val := int(r)
    ret += convertIntToByte(val, base, byteLength)
  }

  return ret
}

func convertByteToDecimal(value string) string {
  ret := 0.0
  revvalue := reverse(value)

  for power, _ := range revvalue {
    val, _ := strconv.Atoi(string(revvalue[power]))
    valf := float64(val)
    pow := math.Pow(2.0, float64(power))
    if pow == 0 {
      pow = 1
    }
    ret += valf*pow
  }

  return strconv.Itoa(int(ret))
}

func convertByteToString(value string, byteLength int) string {
  str := ""
  c := 0

  for i := byteLength; i <= len(value); i += byteLength {
    buf := new(byteLib.Buffer)

    sl, _ := strconv.Atoi(convertByteToDecimal(value[c:i]))

    var data = []interface{}{
  		int16(sl),
  	}

    for _, v := range data {
  		err := binary.Write(buf, binary.LittleEndian, v)
  		if err != nil {
  			fmt.Println("binary.Write failed:", err)
  		}
  	}

    str += string(buf.Bytes())
    c += byteLength
  }

  return str
}

func IsNumber(str string) bool {
  nConv, _  := strconv.Atoi(str)
  sConv := strconv.Itoa(nConv)

  return str == sConv
}

func isByte(str string) bool {
  for i, _ := range str {
    nConv, _ := strconv.Atoi(string(str[i]))
    if nConv > 1 {
      return false
    }
  }
  return true
}

func ConvertStringToNumber(str string) int {
  n, err := strconv.Atoi(str)
  if err != nil {
    fmt.Println("ERROR in ConvertStringToNumber : ", err)
  }
  return n
}

func reverse(s string) string {
    r := []rune(s)
    for i, j := 0, len(r)-1; i < len(r)/2; i, j = i+1, j-1 {
        r[i], r[j] = r[j], r[i]
    }
    return string(r)
}

func balanceByte(value string, byteLength int) string {
  l := len(value)
  str := value

  for ;(l % byteLength) > 0; l++ {
    str += "0"
  }
  return reverse(str)
}

func(byteNumber *ByteNumber) getBytesLength() {
  byteNumber.NumberOfBytes = len(byteNumber.Value)/byteNumber.ByteLength
}

func(byteNumber *ByteNumber) ToString(boundaries int) string {
  str := ""
  l := len(byteNumber.Value)

  for c := 0; c < l; c++ {
    if c%boundaries == 0 && c > 0 {
      str += "-"
    }
    str += string(byteNumber.Value[c])
  }

  return str
}

func(byteNumber *ByteNumber) FormatTo(conversionType string, value interface{}) {

  switch conversionType {

  case "decimal/binary" :

    switch value.(type) {
    case int :
      byteNumber.Value = convertIntToByte(value.(int), byteNumber.Base, byteNumber.ByteLength)
      byteNumber.getBytesLength()
    case float64 :
      fmt.Println("float64")
    }

  case "text/binary" :
    byteNumber.Value = convertStringToByte(value.(string), byteNumber.Base, byteNumber.ByteLength)
    byteNumber.getBytesLength()

  case "binary/decimal" :
    if isByte(value.(string)) {
      byteNumber.Value = convertByteToDecimal(value.(string)) + "\n"
    } else {
      byteNumber.Value = value.(string) + " is not a binary\n"
    }
  case "binary/text" :
    if isByte(value.(string)) {
      byteNumber.Value = convertByteToString(value.(string), byteNumber.ByteLength) + "\n"
    } else {
      byteNumber.Value = value.(string) + " is not a binary\n"
    }
  }
}
