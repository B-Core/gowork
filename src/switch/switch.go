package main

import (
  "fmt"
  "runtime"
  "time"
)

func main() {
  fmt.Print("Go runs on")

  switch os := runtime.GOOS; os {
  case "darwin" :
    fmt.Print("OS X")
  case "linux" :
    fmt.Print("Linux")
  default :
    fmt.Printf(" %v\n", os)
  }

  fmt.Println("When's Saturday ?")

  today := time.Now().Weekday()

  switch time.Saturday {
  case today + 0 :
    fmt.Println("Today")
  case today + 1 :
    fmt.Println("Tomorrow")
  default :
    dayTillSaturday := 0
    for {
      if(int(today) + dayTillSaturday == int(time.Saturday)) {
        fmt.Printf("In %v days\n", dayTillSaturday)
        break
      }
      dayTillSaturday++
    }
  }

  t := time.Now()
  
	switch {
	case t.Hour() < 12:
		fmt.Println("Good morning!")
	case t.Hour() < 17:
		fmt.Println("Good afternoon.")
	default:
		fmt.Println("Good evening.")
	}
}
