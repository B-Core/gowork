package main

import(
  "fmt"
  "reflect"
)

func main() {
  /**
  * A value in GO always store is Type and Value
  * so with the reflect package we can extract this information
  * TypeOf accepts an interface and returns a type : https://golang.org/pkg/reflect/#TypeOf
  */
  x := 3.4
  fmt.Println("type : ", reflect.TypeOf(x), " value : ", reflect.ValueOf(x))

  /**
  * Value has a Type method that returns the Type of a reflect.Value
  * both Type and Value have a Kind method that returns a constant indicating what sort of item is
  * stored: Uint, Float64, Slice, and so on. Also methods on Value with names like Int and Float let
  * us grab values (as int64 and float64) stored inside
  */

  v := reflect.ValueOf(x)

  fmt.Println("type : ", v.Type())
  fmt.Println("Kind is float64 : ", v.Kind() == reflect.Float64)
  fmt.Println("value : ", v.Float())

  /**
  * THE FIRST LAW OF REFLECTION
  * 1. Reflection goes from interface value to reflection object.
  *
  * PROPERTY 1
  * to keep the API simple, the "getter" and "setter" methods of Value operate on the largest type
  * that can hold the value: int64 for all the signed integers, for instance. That is,
  * the Int method of Value returns an int64 and the SetInt value takes an int64;
  * it may be necessary to convert to the actual type involved:
  */

  var y uint8 = 'x' // single quotes return the code value for letter
  fmt.Println(y)
  v = reflect.ValueOf(y)
  fmt.Println("type : ", v.Type())
  fmt.Println("Kind is uint8 : ", v.Kind() == reflect.Uint8)
  fmt.Println("But Uint method of reflect.Type returns a", reflect.TypeOf(v.Uint()))

  /**
  * PROPERTY 2
  * Kind of a reflection object describes the underlying type, not the static type.
  * If a reflection object contains a value of a user-defined integer type.
  * the Kind of v is still reflect.Int, even though the static type of x is MyInt, not int.
  * In other words, the Kind cannot discriminate an int from a MyInt even though the Type can.
  */

  type MyInt int
  var myX MyInt = 7
  v = reflect.ValueOf(myX)
  fmt.Println("Kind of v is Int : ", v.Kind() == reflect.Int)

  /**
  * THE SECOND LAW OF REFLECTION
  * 2. Reflection goes from reflection object to interface value.
  * Given a reflect.Value we can recover an interface value using the Interface method;
  * in effect the method packs the type and value information back into
  * an interface representation and returns the result
  *
  * he empty interface value has the concrete value's type information inside
  * and fmt will recover it.
  */
  v = reflect.ValueOf(x)
  interfaceV := v.Interface().(float64) // interfaceV will have the type float64
  fmt.Println(interfaceV)
  fmt.Println(v, "typeOf v :", reflect.TypeOf(v))
  fmt.Println(v.Interface(), "typeOf v.Interface() :", reflect.TypeOf(v.Interface())) // Prints float64 because fmt converts the value form inside


  /**
  * THE THIRD LAW OF REFLECTION
  * 3. To modify a reflection object, the value must be settable.
  */

  // this code will panic
  // var rx float64 = 3.4
  // rv := reflect.ValueOf(rx)
  // rv.SetFloat(7.1)

  /**
  * The problem is not that the value 7.1 is not addressable; it's that v is not settable.
  * Settability is a property of a reflection Value, and not all reflection Values have it.
  * The CanSet method of Value reports the settability of a Value
  */

  var rx float64 = 3.4
  rv := reflect.ValueOf(rx)
  fmt.Println("settability of rv : ", rv.CanSet())

  /**
  * rv is just a copy of rx, not rx itself
  * so to modify rv directly through reflection, we must pass to a pointer to the reflection
  */

  var initVar float64 = 3.4
  // we pass a reference to the value initVar to the reflection
  // thus what it is passed is not the value itself, but the address to that value
  pointerReflection := reflect.ValueOf(&initVar)
  fmt.Println("type of pointerReflection = ", pointerReflection.Type())
  fmt.Println("pointerReflection is settable :", pointerReflection.CanSet())

  /**
  * The reflection object pointerReflection isn't settable, but it's not pointerReflection we want to set,
  * it's (in effect) *pointerReflection.
  * To get to what pointerReflection points to, we call the Elem method of Value, which indirects through the pointer
  */

  settableReflection := pointerReflection.Elem()
  fmt.Println("settableReflection is settable :", settableReflection.CanSet())

  /**
  * and since it represents initVar, we are finally able to use
  * setttableReflection.SetFloat to modify the value of initVar
  */

  settableReflection.SetFloat(7.2);
  fmt.Println(settableReflection.Interface())
  fmt.Println(initVar)

  /**
  * Reflection can be hard to understand but it's doing exactly what the language does,
  * albeit through reflection Types and Values that can disguise what's going on.
  * Just keep in mind that reflection Values need the address of something in order to modify what they represent.
  */

}
