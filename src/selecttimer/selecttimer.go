package main

import (
  "fmt"
  "time"
  "os"
  "strconv"
)

func PrintTimer(second time.Duration) {
  ticker := time.NewTicker(time.Second)
  boom := time.After(second * time.Second)

  for {
    select {
    case <- ticker.C :
      fmt.Println("tick")
    case <- boom :
      fmt.Println("BOOM !")
      ticker.Stop()
      return
    default :
      fmt.Println(".")
      time.Sleep(50 * time.Millisecond)
    }
  }

}

func main() {
  if len(os.Args) >= 2 {
    convertedArgs, _ := strconv.Atoi(os.Args[1])
    timeTillBoom := time.Duration(convertedArgs)
    PrintTimer(timeTillBoom)
  } else {
    fmt.Println("you must provide a duration in second")
  }
}
