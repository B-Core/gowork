package wikipages

import(
  "io/ioutil"
)

const path = "./src/wikipages/pages/"

type Page struct {
  Title string
  Body []byte
}

/**
* Save the page in a text File
* Takes a pointer to a Page as a Receiver (to be able to change its content) and execute ioutil.WriteFile
*/

func (p *Page) Save() error {
  filename := path + p.Title + ".txt"
  /**
  * Save the page in a text File
  * @params
  * filename {string} - the path to the file and its extension
  * data {[]byte} - the byte of the file to write
  * code {os.FileMode} - the permission code, 0600 means Read/Write
  */
  return ioutil.WriteFile(filename, p.Body, 0600)
}

/**
* load the page from a text File
* The function loadPage constructs the file name from the title parameter,
* reads the file's contents into a new variable body,
* and returns a pointer to a Page literal constructed with the proper title and body values.
* return &Page
*/

func LoadPage(title string) (*Page, error) {
  filename := path + title + ".txt"

  body, err := ioutil.ReadFile(filename)

  if err != nil {
    return nil, err
  }

  return &Page{Title : title, Body : body}, nil
}
