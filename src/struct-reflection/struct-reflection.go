package main

import(
  "fmt"
  "reflect"
)

/**
* analyzing the law of reflection applied to Struct
* method and docs : http://golang.org/pkg/reflect/
* article : https://blog.golang.org/laws-of-reflection
*/

// We create a sample Struct
type sampleStruct struct {
  A int
  B string
}

func main() {
  sample := sampleStruct{25, "Ciro"}

  fmt.Println("sample is : ", sample)

  // We create a reflection of the variable using the Struct and get its Elem()
  // to be able to modify it afterwards
  sampleReflection := reflect.ValueOf(&sample).Elem()

  // we loop over the fields of the sampleStruct and print them
  for i := 0; i < sampleReflection.NumField(); i++ {
    // passing a number to the Field function return the field at the index position
    field := sampleReflection.Field(i)
    nameOfField := sampleReflection.Type().Field(i).Name
    fmt.Printf("%d: %s %s = %v\n", i, nameOfField, field.Type(), field.Interface())
  }

  /**
  * There's one more point about settability introduced in passing here:
  * the field names of sampleStruct are upper case (exported) because only exported fields of a struct are settable.
  * Because sampleReflection contains a settable reflection object, we can modify the fields of the structure
  */

  sampleReflection.Field(0).SetInt(52)
  sampleReflection.Field(1).SetString("another person")
  fmt.Println("sample is now : ", sample)

  /**
  * If we modified the program so that sampleReflection was created from sample, not &sample,
  * the calls to SetInt and SetString would fail as the fields of sample would not be settable
  */

  /**
  * RESUME
  * 1 : Reflection goes from interface value to reflection object.
  * 2 : Reflection goes from reflection object to interface value.
  * 3 : To modify a reflection object, the value must be settable.
  */
}
