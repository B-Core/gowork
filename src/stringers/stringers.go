package main

import "fmt"

type Person struct {
  Name string
  Age int
}

// defining a function String on struct or other
// define it as describable via fmt as a string
// you then can describe how it should display
func (p Person) String() string {
  return fmt.Sprintf("%v is %v years old", p.Name, p.Age)
}

func main() {
  Ciro := Person{"Ciro DE CARO", 25}
  Laure := Person{"Laure GASCON", 24}

  fmt.Println(Ciro, Laure)
}
