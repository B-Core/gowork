package main

import (
  "fmt"
  "log"
  "io/ioutil"
  "net/http"
)

func connect(address string) []byte {
  res, err := http.Get(address)
	if err != nil {
		log.Fatal(err)
	}
	results, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		log.Fatal(err)
	}
	return results
}

func worker(id int, url string, jobs chan []byte, results chan []byte) {
  for j := range jobs {
    fmt.Println("worker", id, "processing job", url)
    results <- j
  }
}

func main() {
  uris := []string{
    "https://golang.org",
    "https://google.com",
    "http://cirodecaro.net",
  }

  jobs, results := make(chan []byte), make(chan []byte)

  for wId, url := range uris {
    go worker(wId, url, jobs, results)
    jobs <- connect(url)
  }
  close(jobs)

  for range uris {
    fmt.Printf("%s\n", <-results)
  }
}
