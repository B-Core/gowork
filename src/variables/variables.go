package main

import "fmt"

// variable can be declared in package
// variables can be factorised

var first, second int = 1, 2

func main() {
  // variables in function can be written differently
  // if the := notation is used, type is implicit apparently
  third := 3
  fmt.Print(first + second)
  fmt.Print(" = ")
  fmt.Println(third)
}
