package main

import "fmt"

func add(base, toAdd interface{}) interface{} {
  switch t := base.(type) {
  case string :
    ret := t + toAdd.(string)
    return ret
  case int :
    ret := t + toAdd.(int)
    return ret
  case float64 :
    ret := t + toAdd.(float64)
    return ret
  default :
    return "no type specified in interface"
  }
}

func main() {
  var number interface{} = 2
  var texte interface{} = "test"
  var fl interface{} = 2.354

  addition := add(number, 2)
  texteEdded := add(texte, "2")
  floatAdded := add(fl, 3.256)

  fmt.Println(addition)
  fmt.Println(texteEdded)
  fmt.Println(floatAdded)
}
