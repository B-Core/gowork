package main

import (
	"image"
  "pic"
	"image/color"
	"os"
  "fmt"
  "strconv"
)

type Image struct{
	x, y, Width, Height int
	color uint8
}

func(i *Image) Bounds() image.Rectangle {
	return image.Rect(i.x, i.y, i.Width, i.Height)
}

func(i *Image) ColorModel() color.Model {
	return color.RGBAModel
}

func(i *Image) At(x, y int) color.Color {
	return color.RGBA{i.color*uint8(x), i.color*uint8(y), 255, 255}
}

func main() {
  if len(os.Args) >= 3 {
    imageName := os.Args[1]
    clrInt, _ := strconv.Atoi(os.Args[2])
    colorPalette := uint8(clrInt)

    m := Image{0, 0, 100, 100, colorPalette}
    pic.ShowImage(imageName, &m)
  } else {
    fmt.Println("you must specify an image name and a color palette")
  }
}
