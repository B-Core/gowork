package main

import (
  "fmt"
  "word"
)

type Coordinate struct {
  lat, lon float64
}

// maps are like Object in JS for examples
// Arrays of keys mapped to values which can be typed
// The make function returns a map of the given type, initialized and ready for use.
var coordsMap = make(map[string]Coordinate)

// maps can be litteral to
var litteralMap = map[string]Coordinate{
	"Bell Labs": {
		40.68433, -74.39967,
	},
	"Google": {
		37.42202, -122.08408,
	},
}

func main() {
  coordsMap["Bell Labs"] = Coordinate{
    40.68433, -74.39967,
  }
  fmt.Println(coordsMap["Bell Labs"])
  fmt.Println(litteralMap["Bell Labs"],litteralMap["Google"])

  // deleting a key
  delete(litteralMap, "Bell Labs");

  // check if a key exists
  // it is done wit ha two value assignement
  // If key is in map, ok is true. If not, ok is false.
  // If key is not in the map, then elem is the zero value for the map's element type.
  elem, ok := litteralMap["Bell Labs"]

  fmt.Printf("elem = %v, ok = %v\n", elem, ok)

  elem, ok = litteralMap["Google"]

  fmt.Printf("elem = %v, ok = %v\n", elem, ok)

  fmt.Println(word.Count("I ate a donut. Then I ate another donut."))
}
