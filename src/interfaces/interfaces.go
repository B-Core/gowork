package main

import "fmt"

type MathMethods interface {
  add(float64)
  sub(float64)
  div(float64)
  mult(float64)
  get() MyFloat
}

type MyFloat float64

func (f *MyFloat) add(toAdd float64) {
  *f += MyFloat(toAdd)
}

func (f *MyFloat) get() MyFloat {
  return *f
}

func (f *MyFloat) sub(toSum float64) {
  *f -= MyFloat(toSum)
}

func (f *MyFloat) div(toDiv float64) {
  *f /= MyFloat(toDiv)
}

func (f *MyFloat) mult(toMult float64) {
  *f *= MyFloat(toMult)
}


func main() {
  var numb MathMethods
  lol := MyFloat(3.14)
  numb = &lol

  numb.add(10.5214)

  fmt.Println(numb.get())

  numb.div(2.0)

  fmt.Println(numb.get())
  fmt.Println(lol)
}
