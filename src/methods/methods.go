package main

import (
	"fmt"
	"math"
)

// methods are function with a receiver argument
// they can be declared in any type that is in the same package
type Vertex2D struct {
  x, y float64
}

type MyFloat float64

func(f MyFloat) Abs() float64 {
  if f < 0 {
    return float64(-f)
  }
  return float64(f)
}

func (vertex *Vertex2D) Abs() float64 {
  return math.Sqrt(vertex.x*vertex.x + vertex.y*vertex.y)
}

// With a value receiver, the Scale method operates on a copy of the original Vertex value.
// (This is the same behavior as for any other function argument.)
// The Scale method must have a pointer receiver to change the Vertex value declared in the main function.
func (vertex *Vertex2D) Scale(factor float64) {
  vertex.x = vertex.x * factor
  vertex.y = vertex.y * factor
}

func main() {
  v := Vertex2D{3, 4}
  v2 := Vertex2D{3, 4}
  f := MyFloat(-math.Sqrt2)
  fmt.Println(v.Abs())
  fmt.Println(f.Abs())
  v2.Scale(10)
  fmt.Println(v2.Abs())
}
