package main

import "fmt"

// a struct is a collection of field defining a new custom type
type Vertex2D struct {
  x int
  y int
}

// A struct literal denotes a newly allocated struct value by listing the values of its fields.
// You can list just a subset of fields by using the Name: syntax. (And the order of named fields is irrelevant.)
// The special prefix & returns a pointer to the struct value.

var (
	v1 = Vertex2D{1, 2}  // has type Vertex
	v2 = Vertex2D{x: 1}  // y:0 is implicit
	v3 = Vertex2D{}      // x:0 and Y:0
	vp  = &Vertex2D{1, 2} // has type *Vertex
)

func main() {
  vert := Vertex2D{3,5}
  fmt.Printf("Type : %T, Value : %v\n", vert, vert)

  // you can acces individual field with a dot notation
  fmt.Println(vert.x);

  // pointers to struct works without the * to set the value
  // it's like (*p).x for example
  p := &vert
  fmt.Printf("Type : %T, Value : %v\n", *p, *p)
  p.x = 10
  fmt.Printf("New value : %v\n", vert.x)

  fmt.Println(v1, vp, v2, v3)
}
