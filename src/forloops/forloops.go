package main

import "fmt"

func main() {
  var sum int

  // for loops are composed of 3 parts
  // 1 - the init statement executed before the first iteration
  // 2 - the condition statement evaluated before every iteration
  // 3 - the post statement executed after every iteration

  for i := 0; i < 10; i++ {
    sum = i
    if sum == 9 {
      fmt.Printf("sum = %v and loop is finished\n", sum)
    } else {
      fmt.Printf("sum = %v\n", sum)
    }
  }

  // for loops are the Go while
  // because init and post statement can be omitted
  // the variable used in the for loop without statement is initalized to 0
  for sum < 100 {
    sum++
  }

  fmt.Printf("sum = %v after while loop\n", sum)

  // if you omit every statement, the for loop is infinite
  for {
    sum++
    if sum == 200 {
      fmt.Printf("sum = %v after infinite loop\n", sum)
      break
    }
  }
}
