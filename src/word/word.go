package word

import "strings"

func Count(s string) map[string]int {
	mapString := make(map[string]int)
	sArray := strings.Fields(s)

	ok := false

	for _, value := range sArray {
		_, ok = mapString[value]
		if(ok == true) {
			mapString[value]++
		} else {
			mapString[value] = 1
		}
	}
	return mapString
}
