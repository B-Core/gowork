package main

import (
  "pic"
  "math"
)

func GetFactorial(n int) (int) {
	if n < 0 {
		return 0
	}

	if n <= 1 {
		return 1
	}

	return n * GetFactorial(n - 1)
}

func algo(x,y int) int {
  // return x^y+(x+y)/2
  // return x*y
  // return x^y
  // return (x*y)/2
  // return (x+y)/2
  // return int(math.Exp(float64(x*y%10))) % 256
  // return int(math.Exp(float64(x*y%256))) % 256
  // return int(math.Ldexp(float64(x*y%512), 2))
  // return int((math.Sin(float64(x*y))+1)*128)
  // return int((math.Sin(float64(x*y%512))+1)*128)
  // return int((math.Sin(math.Ldexp(float64(x^y),12))+1)*512)
  // return int((math.Sin(math.Logb(float64(x^y)))+1)*512)
  return int((math.Sin(math.Tan(float64(x*y)))+1)*256)
}

func main() {
  pic.Show(algo)
}
