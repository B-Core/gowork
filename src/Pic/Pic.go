package pic

import(
  "bytes"
  // "encoding/base64"
  "fmt"
  "image"
  "image/png"
  "os"
)

func check(e error) {
    if e != nil {
        panic(e)
    }
}

func picArray(dx, dy int, algo func(x, y int) int) [][]uint8 {
	ret := make([][]uint8, dy)
	for i := 0; i < dy; i++ {
		ret[i] = make([]uint8, dx)
		for j := 0; j < dx; j++ {
			ret[i][j] = uint8(algo(i,j))
		}
	}
	return ret
}

func Show(algo func(x,y int) int) {
  const (
		dx = 512
		dy = 512
	)
	data := picArray(dx, dy, algo)
	m := image.NewNRGBA(image.Rect(0, 0, dx, dy))
	for y := 0; y < dy; y++ {
		for x := 0; x < dx; x++ {
			v := data[y][x]
			i := y*m.Stride + x*4
			m.Pix[i] = v
			m.Pix[i+1] = v
			m.Pix[i+2] = 255
			m.Pix[i+3] = 255
		}
	}
	// ShowImage(m)
}

func saveImage(name string, bytes []byte) {
  file, err := os.Create(name)
  check(err)
  file.Write(bytes)
  defer file.Close()
}

func ShowImage(imageName string, m image.Image) {
	var buf bytes.Buffer
	err := png.Encode(&buf, m)
	if err != nil {
		panic(err)
	}
	// enc := base64.StdEncoding.EncodeToString(buf.Bytes())
  // the base64 string
	// fmt.Println("IMAGE:" + enc)

  fmt.Println("done")
  saveImage("./" + imageName, buf.Bytes())
}
