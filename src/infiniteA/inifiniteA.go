package main

import (
	"io"
  "os"
	"strings"
)

type MyReader struct{
	A io.Reader
}

// sets all the byte in the reader to be 65 (65 = A in byte)
func (rot MyReader) Read(p []byte) (int, error) {
	for index := range p {
		p[index] = 65
	}
	return 65, nil
}

func main() {
	s := strings.NewReader("hywhgfwsfgjwytj")
	r := MyReader{s}
	io.Copy(os.Stdout, r)
}
