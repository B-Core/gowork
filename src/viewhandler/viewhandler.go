package viewhandler

import(
  "net/http"
  "html/template"
  "regexp"
  "strings"
  "errors"
  "log"
  "io/ioutil"
  "wikipages"
)

const viewURL = "/pages/"
const editURL = "/edit/"
const saveURL = "/save/"
const defaultURL = "/"
const pathToTemplate = "./src/wikipages/templates/"
const port = ":8080"

// regex validation to ensure security of file read
var validPath = regexp.MustCompile("^/(edit|save|pages)/([a-zA-Z0-9]+)$")

// Initialize template caching by parsing dir by pattern with ParseGlob
var templates = template.Must(template.ParseGlob(pathToTemplate + "*.html"))

func getTitle(w http.ResponseWriter, r *http.Request) (string, error) {
  // slice the url from the slash after edit and check if it is valid
  m := validPath.FindStringSubmatch(r.URL.Path)
  if m == nil {
      http.NotFound(w, r)
      return "", errors.New("Invalid Page Title")
  }
  return m[2], nil // The title is the second subexpression.
}

func loadTemplate(w http.ResponseWriter,templateName string, p *wikipages.Page) {
  // Execute template by name from templates
  err := templates.ExecuteTemplate(w, templateName + ".html", p)

  // The http.Error function sends a specified HTTP response code (in this case "Internal Server Error") and error message

  if err != nil {
    http.Error(w, err.Error(), http.StatusInternalServerError)
  }
}

func MakeHandler(fn func(http.ResponseWriter, *http.Request, string)) http.HandlerFunc {
  return func(w http.ResponseWriter, r *http.Request) {
    title, err := getTitle(w, r)
    if err != nil {
        return
    }
    fn(w, r, title)
  }
}

/**
* A ViewHandler wich can display pages base on urls
*/

func ViewHandler(w http.ResponseWriter, r *http.Request, title string) {
  p, err := wikipages.LoadPage(title)

  if err != nil {
    /**
    * The http.Redirect function adds an HTTP status code of http.StatusFound (302) and a Location header to the HTTP response.
    */
    http.Redirect(w, r, editURL + title, http.StatusFound)
    return
  }

  loadTemplate(w, "page", p)
}

/**
* A EditHandler wich can display a form to handle editing pages
* The function template.ParseFiles will read the contents of edit.html and return a *template.Template.
*
* The method t.Execute executes the template, writing the generated HTML to the http.ResponseWriter. The .Title and .Body dotted identifiers refer to p.Title and p.Body.
*
* Template directives are enclosed in double curly braces. The printf "%s" .Body instruction is a function call that outputs .Body as a string instead of a stream of bytes, the same as a call to fmt.Printf. The html/template package helps guarantee that only safe and correct-looking HTML is generated by template actions. For instance, it automatically escapes any greater than sign (>), replacing it with &gt;, to make sure user data does not corrupt the form HTML.
*/

func EditHandler(w http.ResponseWriter, r *http.Request, title string) {
  p, err := wikipages.LoadPage(title)

  if err != nil {
    p = &wikipages.Page{Title : title}
  }

  loadTemplate(w, "edit", p)
}

func SaveHandler(w http.ResponseWriter, r *http.Request, title string) {
  // Read from the form, with input name = body
  body := r.FormValue("body")

  // The value returned by FormValue is of type string. We must convert that value to []byte before it will fit into the Page struct. We use []byte(body) to perform the conversion.
  newPage := &wikipages.Page{Title : title, Body : []byte(body)}
  err := newPage.Save()

  if err != nil {
    http.Error(w, err.Error(), http.StatusInternalServerError)
    return
  }

  http.Redirect(w, r, viewURL + title, http.StatusFound)
}

func DefaultHandler(w http.ResponseWriter, r *http.Request) {
  pageList := make([]string, 0)
  list, err := ioutil.ReadDir("./src/wikipages/pages/");
  if err != nil {
		log.Fatal(err)
	}
  for _, listEl := range list {
    pageList = append(pageList, strings.Replace(listEl.Name(), ".txt", "", -1))
  }
  // Execute template by name from templates
  err = templates.ExecuteTemplate(w, "list.html", pageList)

  // The http.Error function sends a specified HTTP response code (in this case "Internal Server Error") and error message

  if err != nil {
    http.Error(w, err.Error(), http.StatusInternalServerError)
  }
}

func InitServer() {
  http.HandleFunc(defaultURL, DefaultHandler)
  http.HandleFunc(viewURL, MakeHandler(ViewHandler))
  http.HandleFunc(editURL, MakeHandler(EditHandler))
  http.HandleFunc(saveURL, MakeHandler(SaveHandler))
  log.Fatal(http.ListenAndServe(port, nil))
}
