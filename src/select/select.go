package main

import (
  "fmt"
  "os"
  "strconv"
)

// The select statement lets a goroutine wait on multiple communication operations.
// A select blocks until one of its cases can run, then it executes that case. It chooses one at random if multiple are ready.

func fibonnaci(c, quit chan int) {
  x, y := 0, 1

  for {
    select {
    case c <- x :
      x, y = y, x+y
      fmt.Printf("x = %v, y = %v\n", x, y)
    case <- quit :
      fmt.Println("done")
      return
    }
  }

}

func main() {

  if(len(os.Args) >= 2) {
    c := make(chan int)
    quit := make(chan int)

    numberOfNumbers, _ := strconv.Atoi(os.Args[1])

    go func() {
      for i := 0; i <= numberOfNumbers; i++ {
        fmt.Println(<-c)
      }
      quit <- 0
    }()
    fibonnaci(c, quit)
  } else {
    fmt.Println("you must pass how many numbers from fibonnaci you want")
  }

}
