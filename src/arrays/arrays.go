package main

import "fmt"

// Arrays have a defined size and cannot be resized
var a [2]string

func printSlice(s []int) {
	fmt.Printf("len=%d cap=%d %v\n", len(s), cap(s), s)
}

func Pic(dx, dy int) [][]uint8 {
  imageArray := make([][]uint8, dy)

  for i := 0; i < dy; i++ {
    imageArray[i] = make([]uint8, dx)

    for j := 0; j < dx; j++ {
      imageArray[i][j] = uint8((i+j)/2)
    }
  }
  return imageArray
}

func main() {
  a[0] = "Hello"
  a[1] = " World"
  fmt.Println(a[0], a[1])
  fmt.Println(a)

  // you can declare the dat of the array directly after defining it
  // surrounding the values with {}
  primes := [6]int{2, 3, 5, 7, 11 ,13}
  fmt.Println(primes)

  // A slice, on the other hand, is a dynamically-sized, flexible view into the elements of an array.
  // In practice, slices are much more common than arrays.
  // The type []T is a slice with elements of type T.

  var s []int = primes[0:5] // slice of the 5 first values of the array primes
  fmt.Println(s);

  // A slice does not store any data, it just describes a section of an underlying array.
  // Changing the elements of a slice modifies the corresponding elements of its underlying array.
  // Other slices that share the same underlying array will see those changes.

  names := [4]string{
		"John",
		"Paul",
		"George",
		"Ringo",
	}
	fmt.Println(names)

	a := names[0:2]
	b := names[1:3]
	fmt.Println(a, b)

	b[0] = "XXX"
	fmt.Println(a, b)
	fmt.Println(names)

  // to creates dynamically sized arrays
  // you create a slice
  dyn := []string{
    "Hello",
    " World",
    " !\n",
  }

  for c, dynLength := 0, len(dyn); c < dynLength ; c++ {
    fmt.Print(dyn[c])
  }

  // to append more objects to a slice, use the append function
  // if more capacity is needed, the append functions extends the capacity og the slice and returns it
  dynArray := make([]int, 3)
  printSlice(dynArray);

  dynArray = append(dynArray, 0,1,2);
  printSlice(dynArray);

  // you can iterate with a for loop on a range of a slice
  // in this case, two values are used
  // first is the index, second is a copy of the element at that index
  iterableArray := []int{1,2,3,4,5,6,7,8,9,10}

  for index, value := range iterableArray {
    fmt.Printf("index = %v, value = %v\n", index, value)
  }
}
