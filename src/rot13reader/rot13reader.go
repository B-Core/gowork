package main

import (
	"io"
	"os"
  "fmt"
	"strings"
)

// ROT13 ("rotate by 13 places", sometimes hyphenated ROT-13) is a simple letter substitution cipher
// that replaces a letter with the letter 13 letters after it in the alphabet.

type rot13Reader struct {
	r io.Reader
}

func(rot *rot13Reader) Read(p []byte) (n int, err error) {
  n, err = rot.r.Read(p)

  for index := range p {
    if (p[index] >= 65 && p[index] < 78) || (p[index] >= 97 && p[index] < 110) {
      p[index] += 13
    } else if (p[index] > 77 && p[index] <= 90 ) || (p[index] > 109 && p[index] <= 122) {
      p[index] -= 13
    }
  }

  return
}

func main() {
  if len(os.Args) >= 2 {
    sentence := os.Args[1]

    s := strings.NewReader(sentence)
    encode := rot13Reader{s}
    io.Copy(os.Stdout, &encode)
  } else {
    fmt.Println("you must specify a sentence to transform")
  }
}
