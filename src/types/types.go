package main

import (
  "fmt"
  "math"
  "strconv"
)

// there is different variables type in go
// each numeric type has a 64 equivalent
var (
  toBe bool = false
  text string = "Test"
  myAge int = 25
  myAgeUInt uint32 = 25
  PI float32 = 3.14
)

// go has constant variables which can't be changed
const bigPI float64 = math.Pi


func main() {
  // variables can be converted from one type to another
  // function to converte is usually the type name with ()
  // except for string conversion where a package is needed, strconv
  convertedAge := strconv.Itoa(myAge)
  convertedBigPi := uint32(bigPI)

  fmt.Printf("Type : %T Value : %v\n",toBe, toBe)
  fmt.Printf("Type : %T Value : %v\n",text, text)
  fmt.Printf("Type : %T Value : %v\n",myAge, myAge)
  fmt.Printf("Type : %T Value : %v\n",convertedAge, convertedAge)
  fmt.Printf("Type : %T Value : %v\n",myAgeUInt, myAgeUInt)
  fmt.Printf("Type : %T Value : %v\n",PI, PI)
  fmt.Printf("Type : %T Value : %v\n",bigPI, bigPI)
  fmt.Printf("Type : %T Value : %v\n",convertedBigPi, convertedBigPi)
}
