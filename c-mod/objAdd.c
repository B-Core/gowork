#include <stdlib.h>
#include "objAdd.h"

void add(t_hello *val, float val2) {
    val->x += val2;
}

t_hello *newHello(float val) {
  t_hello *new;

  new = malloc(sizeof(t_hello));
  new->x = val;
  new->add = &add;
  return (new);
}
