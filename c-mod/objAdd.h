#ifndef _OBJADD_H_
# define _OBJADD_H_

#include <stdlib.h>

typedef struct s_hello {
  float x;
  void (*add)(struct s_hello*, float);
}              t_hello;

t_hello *newHello(float);

# endif
