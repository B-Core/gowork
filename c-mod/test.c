#include <stdio.h>
#include "objAdd.h"

int main(int ac, char **av) {
  (void)ac;
  (void)av;

  t_hello *val = newHello(3.14);
  t_hello *val2 = newHello(10);

  val->add(val, 10);
  val2->add(val2, 2);
  printf("%f\n", val->x);
  printf("%f\n", val2->x);

  free(val);
  free(val2);
  return(0);
}
